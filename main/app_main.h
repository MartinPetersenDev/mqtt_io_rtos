/**
 * MQTT_IO_RTOS
 * 
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev 
 * Apache License Version 2.0
 * 
 * */

#ifndef APP_MAIN_H_
#define APP_MAIN_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include <sys/param.h>

#ifndef TEST
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_log.h"
#include "esp_netif.h"
#include "esp_event.h"
#include "protocol_common.h"
#include "nvs.h"
#include "nvs_flash.h"
#include "lwip/sockets.h"
#include "lwip/dns.h"
#include "lwip/netdb.h"
#include "mqtt_client.h"
#include "driver/gpio.h"

#endif // TEST

#include "../components/include/common.h"

/**
 * GPIO configuration
 */
#define GPIO_OUTPUT_0 12
#define GPIO_OUTPUT_1 14
#define GPIO_OUTPUT_PIN_SEL ((1ULL << GPIO_OUTPUT_0) | (1ULL << GPIO_OUTPUT_1))
#define GPIO_INPUT_0 5
#define GPIO_INPUT_1 4
#define GPIO_INPUT_PIN_SEL ((1ULL << GPIO_INPUT_0) | (1ULL << GPIO_INPUT_1))
#define SIZE_INPUT_ARRAY 3

/**
 * MQTT configuration
 */
#define TAG "MQTT_IO_RTOS"
#define MQTT_DEVICE_ID "wemos" // MAX 18 characters - MQTT_DEVICE_ID will be appended with last 3 bytes of MAC address
#define MQTT_TOPIC "/io/"
#define MQTT_QOS 2 // 0, 1 or 2
#define MAX_JSON_SIZE 150
#define MAX_TOKENS 50
#define MAX_KEY_LENGTH 25
#define JSMN_STRICT

/**
 * Key/value strings
 */
#define DEVICE_ID "device_id"
#define INPUT0 "input0"
#define INPUT1 "input1"
#define OUTPUT0 "output0"
#define OUTPUT1 "output1"
#define ON "on"
#define OF "off"
#define TOGGLE "toggle"

void app_main(void);

#endif /* APP_MAIN_H_ */