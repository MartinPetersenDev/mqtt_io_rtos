/**
 * MQTT_IO_RTOS
 * 
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev 
 * Apache License Version 2.0
 * 
 * */

#include "app_main.h"
#include "../components/gpio_input_task/gpio_input_task.h"
#include "../components/gpio_output_task/gpio_output_task.h"
#include "../components/mqtt_task/mqtt_task.h"
#include "../components/httpd_task/httpd_task.h"
#include "../components/filesystem/filesystem.h"
#include "../components/adc_task/adc_task.h"

/**
 * Main
 */
void app_main()
{

    // Platform setup
    ESP_ERROR_CHECK(nvs_flash_init());
    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    // SPIFFS filesystem setup
    init_filesystem();

    // Get device_config from filesystem
    char broker_ip[16];
    get_device_config(broker_ip);

    // WIFI connect
    ESP_ERROR_CHECK(wifi_connect());

    // Configure logging
    ESP_LOGI(TAG, "[APP] Startup..");
    ESP_LOGI(TAG, "[APP] Free memory: %d bytes", esp_get_free_heap_size());
    ESP_LOGI(TAG, "[APP] IDF version: %s", esp_get_idf_version());

    esp_log_level_set("*", ESP_LOG_INFO);
    esp_log_level_set("MQTT_CLIENT", ESP_LOG_VERBOSE);
    esp_log_level_set("TRANSPORT_TCP", ESP_LOG_VERBOSE);
    esp_log_level_set("TRANSPORT_SSL", ESP_LOG_VERBOSE);
    esp_log_level_set("TRANSPORT", ESP_LOG_VERBOSE);
    esp_log_level_set("OUTBOX", ESP_LOG_VERBOSE);

    // GPIO setup
    gpio_config_t io_conf;
    // Outputs
    io_conf.intr_type = GPIO_INTR_DISABLE;      // disable interrupt
    io_conf.mode = GPIO_MODE_OUTPUT;            // set as output mode
    io_conf.pin_bit_mask = GPIO_OUTPUT_PIN_SEL; // bit mask
    io_conf.pull_down_en = 0;                   // disable pull-down mode
    io_conf.pull_up_en = 0;                     // disable pull-up mode
    gpio_config(&io_conf);                      // apply gpio settings
    // Inputs
    io_conf.intr_type = GPIO_INTR_DISABLE;     // disable interrupt
    io_conf.pin_bit_mask = GPIO_INPUT_PIN_SEL; // bit mask
    io_conf.mode = GPIO_MODE_INPUT;            // set as input mode
    io_conf.pull_up_en = 1;                    // enable pull-up mode
    gpio_config(&io_conf);                     // apply gpio settings

    // ADC setup
    adc_config_t adc_config;
    adc_config.mode = ADC_READ_TOUT_MODE;
    adc_config.clk_div = 8; // ADC sample collection clock = 80MHz/clk_div = 10MHz
    ESP_ERROR_CHECK(adc_init(&adc_config));

    // Create queues
    xQueueHandle gpio_input_queue = xQueueCreate(2, sizeof(struct input_t) * SIZE_INPUT_ARRAY);
    xQueueHandle json_queue = xQueueCreate(1, MAX_JSON_SIZE);

    // If broker_ip is defined in device_config, start MQTT - else start webserver
    if (strcmp(broker_ip, "") == 0 || strlen(broker_ip) < 8)
    {
        ESP_LOGW(TAG, "No broker ip configured, starting webserver for configuration");
        start_webserver();
    }
    else
    {
        stop_webserver();
        mqtt_app_start(json_queue, gpio_input_queue, broker_ip);
    }

    // Create other tasks
    xTaskCreate(gpio_input_task, "gpio_input_task", 2048, (void *)gpio_input_queue, 10, NULL);
    xTaskCreate(gpio_output_task, "gpio_output_task", 2048, (void *)json_queue, 10, NULL);
    xTaskCreate(adc_task, "adc_task", 2048, (void *)gpio_input_queue, 10, NULL);
}
