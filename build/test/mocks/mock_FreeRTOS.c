#include <string.h>
#include <stdlib.h>
#include <setjmp.h>
#include "cmock.h"
#include "mock_FreeRTOS.h"


static struct mock_FreeRTOSInstance
{
  unsigned char placeHolder;
} Mock;

extern jmp_buf AbortFrame;
extern int GlobalExpectCount;
extern int GlobalVerifyOrder;

void mock_FreeRTOS_Verify(void)
{
}

void mock_FreeRTOS_Init(void)
{
  mock_FreeRTOS_Destroy();
}

void mock_FreeRTOS_Destroy(void)
{
  CMock_Guts_MemFreeAll();
  memset(&Mock, 0, sizeof(Mock));
  GlobalExpectCount = 0;
  GlobalVerifyOrder = 0;
}

