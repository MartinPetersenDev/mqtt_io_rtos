/* AUTOGENERATED FILE. DO NOT EDIT. */
#ifndef _MOCK_ESP_LOG_H
#define _MOCK_ESP_LOG_H

#include "unity.h"
#include "esp_log.h"

/* Ignore the following warnings, since we are copying code */
#if defined(__GNUC__) && !defined(__ICC) && !defined(__TMS470__)
#if __GNUC__ > 4 || (__GNUC__ == 4 && (__GNUC_MINOR__ > 6 || (__GNUC_MINOR__ == 6 && __GNUC_PATCHLEVEL__ > 0)))
#pragma GCC diagnostic push
#endif
#if !defined(__clang__)
#pragma GCC diagnostic ignored "-Wpragmas"
#endif
#pragma GCC diagnostic ignored "-Wunknown-pragmas"
#pragma GCC diagnostic ignored "-Wduplicate-decl-specifier"
#endif

void mock_esp_log_Init(void);
void mock_esp_log_Destroy(void);
void mock_esp_log_Verify(void);




#define esp_log_set_putchar_IgnoreAndReturn(cmock_retval) esp_log_set_putchar_CMockIgnoreAndReturn(__LINE__, cmock_retval)
void esp_log_set_putchar_CMockIgnoreAndReturn(UNITY_LINE_TYPE cmock_line, putchar_like_t cmock_to_return);
#define esp_log_set_putchar_StopIgnore() esp_log_set_putchar_CMockStopIgnore()
void esp_log_set_putchar_CMockStopIgnore(void);
#define esp_log_set_putchar_ExpectAndReturn(func, cmock_retval) esp_log_set_putchar_CMockExpectAndReturn(__LINE__, func, cmock_retval)
void esp_log_set_putchar_CMockExpectAndReturn(UNITY_LINE_TYPE cmock_line, putchar_like_t func, putchar_like_t cmock_to_return);
typedef putchar_like_t (* CMOCK_esp_log_set_putchar_CALLBACK)(putchar_like_t func, int cmock_num_calls);
void esp_log_set_putchar_AddCallback(CMOCK_esp_log_set_putchar_CALLBACK Callback);
void esp_log_set_putchar_Stub(CMOCK_esp_log_set_putchar_CALLBACK Callback);
#define esp_log_set_putchar_StubWithCallback esp_log_set_putchar_Stub
#define esp_log_timestamp_IgnoreAndReturn(cmock_retval) esp_log_timestamp_CMockIgnoreAndReturn(__LINE__, cmock_retval)
void esp_log_timestamp_CMockIgnoreAndReturn(UNITY_LINE_TYPE cmock_line, uint32_t cmock_to_return);
#define esp_log_timestamp_StopIgnore() esp_log_timestamp_CMockStopIgnore()
void esp_log_timestamp_CMockStopIgnore(void);
#define esp_log_timestamp_ExpectAndReturn(cmock_retval) esp_log_timestamp_CMockExpectAndReturn(__LINE__, cmock_retval)
void esp_log_timestamp_CMockExpectAndReturn(UNITY_LINE_TYPE cmock_line, uint32_t cmock_to_return);
typedef uint32_t (* CMOCK_esp_log_timestamp_CALLBACK)(int cmock_num_calls);
void esp_log_timestamp_AddCallback(CMOCK_esp_log_timestamp_CALLBACK Callback);
void esp_log_timestamp_Stub(CMOCK_esp_log_timestamp_CALLBACK Callback);
#define esp_log_timestamp_StubWithCallback esp_log_timestamp_Stub
#define esp_log_early_timestamp_IgnoreAndReturn(cmock_retval) esp_log_early_timestamp_CMockIgnoreAndReturn(__LINE__, cmock_retval)
void esp_log_early_timestamp_CMockIgnoreAndReturn(UNITY_LINE_TYPE cmock_line, uint32_t cmock_to_return);
#define esp_log_early_timestamp_StopIgnore() esp_log_early_timestamp_CMockStopIgnore()
void esp_log_early_timestamp_CMockStopIgnore(void);
#define esp_log_early_timestamp_ExpectAndReturn(cmock_retval) esp_log_early_timestamp_CMockExpectAndReturn(__LINE__, cmock_retval)
void esp_log_early_timestamp_CMockExpectAndReturn(UNITY_LINE_TYPE cmock_line, uint32_t cmock_to_return);
typedef uint32_t (* CMOCK_esp_log_early_timestamp_CALLBACK)(int cmock_num_calls);
void esp_log_early_timestamp_AddCallback(CMOCK_esp_log_early_timestamp_CALLBACK Callback);
void esp_log_early_timestamp_Stub(CMOCK_esp_log_early_timestamp_CALLBACK Callback);
#define esp_log_early_timestamp_StubWithCallback esp_log_early_timestamp_Stub
#define esp_log_write_Ignore() esp_log_write_CMockIgnore()
void esp_log_write_CMockIgnore(void);
#define esp_log_write_StopIgnore() esp_log_write_CMockStopIgnore()
void esp_log_write_CMockStopIgnore(void);
#define esp_log_write_Expect(level, tag, format) esp_log_write_CMockExpect(__LINE__, level, tag, format)
void esp_log_write_CMockExpect(UNITY_LINE_TYPE cmock_line, esp_log_level_t level, const char* tag, const char* format);
typedef void (* CMOCK_esp_log_write_CALLBACK)(esp_log_level_t level, const char* tag, const char* format, int cmock_num_calls);
void esp_log_write_AddCallback(CMOCK_esp_log_write_CALLBACK Callback);
void esp_log_write_Stub(CMOCK_esp_log_write_CALLBACK Callback);
#define esp_log_write_StubWithCallback esp_log_write_Stub
#define esp_early_log_write_Ignore() esp_early_log_write_CMockIgnore()
void esp_early_log_write_CMockIgnore(void);
#define esp_early_log_write_StopIgnore() esp_early_log_write_CMockStopIgnore()
void esp_early_log_write_CMockStopIgnore(void);
#define esp_early_log_write_Expect(level, tag, format) esp_early_log_write_CMockExpect(__LINE__, level, tag, format)
void esp_early_log_write_CMockExpect(UNITY_LINE_TYPE cmock_line, esp_log_level_t level, const char* tag, const char* format);
typedef void (* CMOCK_esp_early_log_write_CALLBACK)(esp_log_level_t level, const char* tag, const char* format, int cmock_num_calls);
void esp_early_log_write_AddCallback(CMOCK_esp_early_log_write_CALLBACK Callback);
void esp_early_log_write_Stub(CMOCK_esp_early_log_write_CALLBACK Callback);
#define esp_early_log_write_StubWithCallback esp_early_log_write_Stub

#if defined(__GNUC__) && !defined(__ICC) && !defined(__TMS470__)
#if __GNUC__ > 4 || (__GNUC__ == 4 && (__GNUC_MINOR__ > 6 || (__GNUC_MINOR__ == 6 && __GNUC_PATCHLEVEL__ > 0)))
#pragma GCC diagnostic pop
#endif
#endif

#endif
