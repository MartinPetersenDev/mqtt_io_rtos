/* AUTOGENERATED FILE. DO NOT EDIT. */

/*=======Automagically Detected Files To Include=====*/
#include "unity.h"
#include "cmock.h"
#include "mock_esp_spiffs.h"
#include "mock_esp_log.h"

int GlobalExpectCount;
int GlobalVerifyOrder;
char* GlobalOrderError;

/*=======External Functions This Runner Calls=====*/
extern void setUp(void);
extern void tearDown(void);
extern void test_get_device_config_should_fail_when_no_file_found(void);
extern void test_get_device_config_should_fail_when_file_is_empty(void);
extern void test_get_device_config_should_pass_when_file_contains_config(void);
extern void test_get_device_config_should_return_correct_ip_adress_as_string(void);
extern void test_get_device_config_should_pass_if_file_contains_newline_character(void);


/*=======Mock Management=====*/
static void CMock_Init(void)
{
  GlobalExpectCount = 0;
  GlobalVerifyOrder = 0;
  GlobalOrderError = NULL;
  mock_esp_spiffs_Init();
  mock_esp_log_Init();
}
static void CMock_Verify(void)
{
  mock_esp_spiffs_Verify();
  mock_esp_log_Verify();
}
static void CMock_Destroy(void)
{
  mock_esp_spiffs_Destroy();
  mock_esp_log_Destroy();
}

/*=======Test Reset Options=====*/
void resetTest(void);
void resetTest(void)
{
  tearDown();
  CMock_Verify();
  CMock_Destroy();
  CMock_Init();
  setUp();
}
void verifyTest(void);
void verifyTest(void)
{
  CMock_Verify();
}

/*=======Test Runner Used To Run Each Test=====*/
static void run_test(UnityTestFunction func, const char* name, int line_num)
{
    Unity.CurrentTestName = name;
    Unity.CurrentTestLineNumber = line_num;
#ifdef UNITY_USE_COMMAND_LINE_ARGS
    if (!UnityTestMatches())
        return;
#endif
    Unity.NumberOfTests++;
    UNITY_CLR_DETAILS();
    UNITY_EXEC_TIME_START();
    CMock_Init();
    if (TEST_PROTECT())
    {
        setUp();
        func();
    }
    if (TEST_PROTECT())
    {
        tearDown();
        CMock_Verify();
    }
    CMock_Destroy();
    UNITY_EXEC_TIME_STOP();
    UnityConcludeTest();
}

/*=======MAIN=====*/
int main(void)
{
  UnityBegin("test_filesystem.c");
  run_test(test_get_device_config_should_fail_when_no_file_found, "test_get_device_config_should_fail_when_no_file_found", 19);
  run_test(test_get_device_config_should_fail_when_file_is_empty, "test_get_device_config_should_fail_when_file_is_empty", 26);
  run_test(test_get_device_config_should_pass_when_file_contains_config, "test_get_device_config_should_pass_when_file_contains_config", 37);
  run_test(test_get_device_config_should_return_correct_ip_adress_as_string, "test_get_device_config_should_return_correct_ip_adress_as_string", 49);
  run_test(test_get_device_config_should_pass_if_file_contains_newline_character, "test_get_device_config_should_pass_if_file_contains_newline_character", 63);

  CMock_Guts_MemFreeFinal();
  return UnityEnd();
}
