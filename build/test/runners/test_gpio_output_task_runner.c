/* AUTOGENERATED FILE. DO NOT EDIT. */

/*=======Automagically Detected Files To Include=====*/
#include "unity.h"
#include "cmock.h"
#include "mock_esp_spiffs.h"
#include "mock_esp_log.h"
#include "mock_gpio.h"
#include "mock_FreeRTOS.h"

int GlobalExpectCount;
int GlobalVerifyOrder;
char* GlobalOrderError;

/*=======External Functions This Runner Calls=====*/
extern void setUp(void);
extern void tearDown(void);
extern void test_json_to_keyval_should_fail_when_passed_empty_json(void);
extern void test_json_to_keyval_should_fail_when_passed_invalid_json(void);
extern void test_json_to_keyval_should_fail_when_passed_string_json(void);
extern void test_json_to_keyval_should_pass_when_passed_valid_json(void);
extern void test_keyval_to_output_should_return_true_when_passed_on(void);
extern void test_keyval_to_output_should_return_true_when_passed_toggle_on_mocked_false(void);
extern void test_keyval_to_output_should_return_false_when_passed_else(void);


/*=======Mock Management=====*/
static void CMock_Init(void)
{
  GlobalExpectCount = 0;
  GlobalVerifyOrder = 0;
  GlobalOrderError = NULL;
  mock_esp_spiffs_Init();
  mock_esp_log_Init();
  mock_gpio_Init();
  mock_FreeRTOS_Init();
}
static void CMock_Verify(void)
{
  mock_esp_spiffs_Verify();
  mock_esp_log_Verify();
  mock_gpio_Verify();
  mock_FreeRTOS_Verify();
}
static void CMock_Destroy(void)
{
  mock_esp_spiffs_Destroy();
  mock_esp_log_Destroy();
  mock_gpio_Destroy();
  mock_FreeRTOS_Destroy();
}

/*=======Test Reset Options=====*/
void resetTest(void);
void resetTest(void)
{
  tearDown();
  CMock_Verify();
  CMock_Destroy();
  CMock_Init();
  setUp();
}
void verifyTest(void);
void verifyTest(void)
{
  CMock_Verify();
}

/*=======Test Runner Used To Run Each Test=====*/
static void run_test(UnityTestFunction func, const char* name, int line_num)
{
    Unity.CurrentTestName = name;
    Unity.CurrentTestLineNumber = line_num;
#ifdef UNITY_USE_COMMAND_LINE_ARGS
    if (!UnityTestMatches())
        return;
#endif
    Unity.NumberOfTests++;
    UNITY_CLR_DETAILS();
    UNITY_EXEC_TIME_START();
    CMock_Init();
    if (TEST_PROTECT())
    {
        setUp();
        func();
    }
    if (TEST_PROTECT())
    {
        tearDown();
        CMock_Verify();
    }
    CMock_Destroy();
    UNITY_EXEC_TIME_STOP();
    UnityConcludeTest();
}

/*=======MAIN=====*/
int main(void)
{
  UnityBegin("test_gpio_output_task.c");
  run_test(test_json_to_keyval_should_fail_when_passed_empty_json, "test_json_to_keyval_should_fail_when_passed_empty_json", 18);
  run_test(test_json_to_keyval_should_fail_when_passed_invalid_json, "test_json_to_keyval_should_fail_when_passed_invalid_json", 26);
  run_test(test_json_to_keyval_should_fail_when_passed_string_json, "test_json_to_keyval_should_fail_when_passed_string_json", 34);
  run_test(test_json_to_keyval_should_pass_when_passed_valid_json, "test_json_to_keyval_should_pass_when_passed_valid_json", 42);
  run_test(test_keyval_to_output_should_return_true_when_passed_on, "test_keyval_to_output_should_return_true_when_passed_on", 54);
  run_test(test_keyval_to_output_should_return_true_when_passed_toggle_on_mocked_false, "test_keyval_to_output_should_return_true_when_passed_toggle_on_mocked_false", 66);
  run_test(test_keyval_to_output_should_return_false_when_passed_else, "test_keyval_to_output_should_return_false_when_passed_else", 79);

  CMock_Guts_MemFreeFinal();
  return UnityEnd();
}
