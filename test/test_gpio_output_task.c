#include "unity.h"
#include "cmock.h"
#include "gpio_output_task.h"
#include "jsmn.h"
#include "mock_esp_spiffs.h"
#include "mock_esp_log.h"
#include "mock_gpio.h"
#include "mock_FreeRTOS.h"

void setUp(void)
{
}

void tearDown(void)
{
}

void test_json_to_keyval_should_fail_when_passed_empty_json(void)
{
    // Setup variable
    char json[MAX_JSON_SIZE];
    // Assert
    TEST_ASSERT_EQUAL_INT(ESP_FAIL, json_to_keyval(""));
}

void test_json_to_keyval_should_fail_when_passed_invalid_json(void)
{
    // Setup variable
    char json[MAX_JSON_SIZE];
    // Assert
    TEST_ASSERT_EQUAL_INT(ESP_FAIL, json_to_keyval("{invalid}"));
}

void test_json_to_keyval_should_fail_when_passed_string_json(void)
{
    // Setup variable
    char json[MAX_JSON_SIZE];
    // Assert
    TEST_ASSERT_EQUAL_INT(ESP_FAIL, json_to_keyval("string value"));
}

void test_json_to_keyval_should_pass_when_passed_valid_json(void)
{
    // Setup variable
    char json[MAX_JSON_SIZE];

    // Setup mock
    gpio_set_level_IgnoreAndReturn(ESP_OK);

    // Assert
    TEST_ASSERT_EQUAL_INT(ESP_OK, json_to_keyval("{\"output0\":\"on\"}"));
}

void test_keyval_to_output_should_return_true_when_passed_on(void)
{
    // Setup variable
    char json[MAX_JSON_SIZE];

    // Setup mock
    gpio_set_level_IgnoreAndReturn(ESP_OK);

    // Assert
    TEST_ASSERT_EQUAL_INT(1, (int)keyval_to_output(ON, 1));
}

void test_keyval_to_output_should_return_true_when_passed_toggle_on_mocked_false(void)
{
    // Setup variable
    char json[MAX_JSON_SIZE];

    // Setup mock
    gpio_set_level_IgnoreAndReturn(ESP_OK);
    gpio_get_level_IgnoreAndReturn(0);

    // Assert
    TEST_ASSERT_EQUAL_INT(1, (int)keyval_to_output(TOGGLE, 1));
}

void test_keyval_to_output_should_return_false_when_passed_else(void)
{
    // Setup variable
    char json[MAX_JSON_SIZE];

    // Setup mock
    gpio_set_level_IgnoreAndReturn(ESP_OK);

    // Assert
    TEST_ASSERT_EQUAL_INT(0, (int)keyval_to_output("something", 1));
}
