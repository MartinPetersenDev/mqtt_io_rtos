#include "unity.h"
#include "cmock.h"
#include "filesystem.h"
#include "mock_esp_spiffs.h"
#include "mock_esp_log.h"
//#include "mock_FreeRTOS.h"
//#include "mock_task.h"

void setUp(void)
{
}

void tearDown(void)
{
    // Remove test file
    int del = remove(DEVICE_CONFIG_PATH);
}

void test_get_device_config_should_fail_when_no_file_found(void)
{
    char result[16];
    // Assert no file found
    TEST_ASSERT_EQUAL_INT(ESP_FAIL, get_device_config(result));
}

void test_get_device_config_should_fail_when_file_is_empty(void)
{
    char result[16];
    // Create test file
    FILE *f = fopen(DEVICE_CONFIG_PATH, "w");
    fclose(f);

    // Assert empty file found
    TEST_ASSERT_EQUAL_INT(ESP_FAIL, get_device_config(result));
}

void test_get_device_config_should_pass_when_file_contains_config(void)
{
    char result[16];
    // Create test file
    FILE *f = fopen(DEVICE_CONFIG_PATH, "w");
    fprintf(f, "broker=127.0.0.1");
    fclose(f);

    // Assert file exists
    TEST_ASSERT_EQUAL_INT(ESP_OK, get_device_config(result));
}

void test_get_device_config_should_return_correct_ip_adress_as_string(void)
{
    char *expected = "127.0.0.1";
    char result[16];
    // Create test file
    FILE *f = fopen(DEVICE_CONFIG_PATH, "w");
    fprintf(f, "broker=127.0.0.1");
    fclose(f);

    // Assert result
    get_device_config(result);
    TEST_ASSERT_EQUAL_STRING(expected, result);
}

void test_get_device_config_should_pass_if_file_contains_newline_character(void)
{
    char *expected = "127.0.0.1";
    char result[16];
    // Create test file
    FILE *f = fopen(DEVICE_CONFIG_PATH, "w");
    fprintf(f, "broker=127.0.0.1\n");
    fclose(f);

    // Assert result
    get_device_config(result);
    TEST_ASSERT_EQUAL_STRING(expected, result);
}