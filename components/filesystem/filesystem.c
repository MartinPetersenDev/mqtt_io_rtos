/**
 * MQTT_IO_RTOS
 * 
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev 
 * Apache License Version 2.0
 * 
 * */

#include "filesystem.h"

int init_filesystem(void)
{
    ESP_LOGI(TAG, "Initializing SPIFFS");

    esp_vfs_spiffs_conf_t conf = {
        .base_path = "/spiffs",
        .partition_label = NULL,
        .max_files = 5,
        .format_if_mount_failed = true};

    // Use settings defined above to initialize and mount SPIFFS filesystem.
    // Note: esp_vfs_spiffs_register is an all-in-one convenience function.
    esp_err_t ret = esp_vfs_spiffs_register(&conf);

    if (ret != ESP_OK)
    {
        if (ret == ESP_FAIL)
        {
            ESP_LOGE(TAG, "Failed to mount or format filesystem");
            return ESP_FAIL;
        }
        else if (ret == ESP_ERR_NOT_FOUND)
        {
            ESP_LOGE(TAG, "Failed to find SPIFFS partition");
            return ESP_ERR_NOT_FOUND;
        }
        else
        {
            ESP_LOGE(TAG, "Failed to initialize SPIFFS (%s)", esp_err_to_name(ret));
        }
        //return;
    }

    size_t total = 0, used = 0;
    ret = esp_spiffs_info(NULL, &total, &used);
    if (ret != ESP_OK)
    {
        ESP_LOGE(TAG, "Failed to get SPIFFS partition information (%s)", esp_err_to_name(ret));
        return ESP_FAIL;
    }
    else
    {
        ESP_LOGI(TAG, "Partition size: total: %d, used: %d", total, used);
    }

    return ESP_OK;
}

int get_device_config(char *broker_ip)
{
    // Open device_config for reading
    ESP_LOGI(TAG, "Checking for device_config file");
    FILE *f = fopen(DEVICE_CONFIG_PATH, "r");
    if (f != NULL)
    {
        char line[64];
        while (fgets(line, sizeof(line), f))
        {
            ESP_LOGI(TAG, "read line from device_config: %s", line);
            // Split line at token =
            char *token = strtok(line, "=");
            while (token != NULL)
            {
                if (strcmp(token, "broker") == 0)
                {
                    // get next token
                    token = strtok(NULL, "=");
                    int len = strlen(token);
                    //strip newline
                    if (len > 0 && token[len - 1] == '\n')
                    {
                        token[--len] = '\0';
                    }
                    // copy token to broker_ip
                    strcpy(broker_ip, token);
                    ESP_LOGI(TAG, "Found broker ip %s in device_config", broker_ip);
                }
                // get next token
                token = strtok(NULL, "=");
            }
        }
        fclose(f);
    }
    else
    {
        ESP_LOGW(TAG, "No device_config found");
        return ESP_FAIL;
    }

    // Check result - smallest possible ip is 1.1.1.1\0 = 8 chars long
    if (strlen(broker_ip) < 8)
    {
        ESP_LOGE(TAG, "Could not extract broker ip from device_config");
        return ESP_FAIL;
    }
    else
    {
        return ESP_OK;
    }
}