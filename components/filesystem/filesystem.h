/**
 * MQTT_IO_RTOS
 * 
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev 
 * Apache License Version 2.0
 * 
 * */

#ifndef FILESYSTEM_H
#define FILESYSTEM_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "esp_spiffs.h"
#include "../include/common.h"

int init_filesystem(void);
int get_device_config(char *broker_ip);

// Redefine esp_logging as test stub
#ifdef TEST
#undef ESP_LOGI
#undef ESP_LOGW
#undef ESP_LOGE
#define ESP_LOGI(tag, format, ...) printf("%s\n", format)
#define ESP_LOGW(tag, format, ...) printf("%s\n", format)
#define ESP_LOGE(tag, format, ...) printf("%s\n", format)
#endif
#endif /* FILESYSTEM_H */