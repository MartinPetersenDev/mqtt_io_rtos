/**
 * MQTT_IO_RTOS
 * 
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev 
 * Apache License Version 2.0
 * 
 * */

#include "mqtt_task.h"

// static GLOBAL variables
static xQueueHandle json_message_queue;
static volatile esp_mqtt_client_handle_t mqtt_client;
static char global_device_id[25];

/**
 * Helper function to get deviceid from MQTT_DEVICE_ID and MAC
 */
void get_device_id(char *device_id)
{
    // get default mac
    uint8_t esp_mac[13];
    esp_efuse_mac_get_default(esp_mac);
    // concatenate MQTT_DEVICE_ID and last 3 bytes of MAC
    sprintf(device_id, "%s%x%x%x", MQTT_DEVICE_ID, esp_mac[3], esp_mac[4], esp_mac[5]);
}

/**
 * Task for handling MQTT publishing of GPIO input states
 */
void mqtt_publish_task(void *arg)
{
    xQueueHandle ptr_gpio_input_queue = (xQueueHandle)arg;

    // set device id
    get_device_id(global_device_id);

    // set topic
    static char topic_input[35];
    sprintf(topic_input, "%s%s/input", MQTT_TOPIC, global_device_id);

    // Variables
    struct input_t input_array[SIZE_INPUT_ARRAY];
    char json[MAX_JSON_SIZE];

    // Task setup
    const TickType_t xFrequency = 200 / portTICK_RATE_MS;
    TickType_t xLastWakeTime;
    xLastWakeTime = xTaskGetTickCount();

    // Task loop
    while (1)
    {
        if (xQueueReceive(ptr_gpio_input_queue, &input_array, 0))
        {
            // Clear json buffer
            memset(json, 0, sizeof(json));

            // Queue contains gpio
            if (input_array[0].input_type == INPUT_IS_GPIO && input_array[1].input_type == INPUT_IS_GPIO)
            {
                snprintf(json,
                         MAX_JSON_SIZE,
                         "{\"device_id\":\"%s\",\"input0\":\"%s\",\"input1\":\"%s\",\"counter0\":%d,\"counter1\":%d}",
                         global_device_id,
                         input_array[0].value ? "on" : "off",
                         input_array[1].value ? "on" : "off",
                         input_array[0].int_value,
                         input_array[1].int_value);
            }
            // Queue contains adc
            else if (input_array[2].input_type == INPUT_IS_ADC)
            {
                snprintf(json,
                         MAX_JSON_SIZE,
                         "{\"device_id\":\"%s\",\"analog0\":%d}",
                         global_device_id,
                         input_array[2].int_value);
            }

            // Log sent json
            ESP_LOGI(TAG, "[mqtt_publish_task] Send json=%s", json);

            // Publish json
            if (json[0] != '\0')
            {
                int msg_id = esp_mqtt_client_publish(mqtt_client, topic_input, json, 0, MQTT_QOS, 0);
                ESP_LOGI(TAG, "sent publish successful, msg_id=%d", msg_id);
            }
        }

        vTaskDelayUntil(&xLastWakeTime, xFrequency);
    }
}

/**
 * MQTT event handler
 */
esp_err_t mqtt_event_handler(esp_mqtt_event_handle_t event)
{
    // Variables
    esp_mqtt_client_handle_t client = event->client;
    int msg_id;

    // Handle events
    switch (event->event_id)
    {
    case MQTT_EVENT_CONNECTED:
        ESP_LOGI(TAG, "MQTT_EVENT_CONNECTED");
        // set topic
        static char topic_output[35];
        sprintf(topic_output, "%s%s/output", MQTT_TOPIC, global_device_id);
        // subscribe to topic
        msg_id = esp_mqtt_client_subscribe(client, topic_output, MQTT_QOS);
        ESP_LOGI(TAG, "sent subscribe successful, msg_id=%d", msg_id);
        break;
    case MQTT_EVENT_DISCONNECTED:
        ESP_LOGI(TAG, "MQTT_EVENT_DISCONNECTED");
        break;
    case MQTT_EVENT_SUBSCRIBED:
        ESP_LOGI(TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
        break;
    case MQTT_EVENT_UNSUBSCRIBED:
        ESP_LOGI(TAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
        break;
    case MQTT_EVENT_PUBLISHED:
        ESP_LOGI(TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
        break;
    case MQTT_EVENT_DATA:
        ESP_LOGI(TAG, "MQTT_EVENT_DATA received, length=%d", event->data_len);
        // Put JSON data on queue
        char data[MAX_JSON_SIZE];
        snprintf(data, event->data_len + 1, event->data);
        xQueueSend(json_message_queue, data, 0);
        break;
    case MQTT_EVENT_ERROR:
        ESP_LOGI(TAG, "MQTT_EVENT_ERROR");
        break;
    }
    return ESP_OK;
}

/** 
 * MQTT application start
 */
void mqtt_app_start(xQueueHandle *json_queue, xQueueHandle *input_queue, char *broker_ip)
{
    // Assign to static global variable
    json_message_queue = json_queue;

    // Built broker url
    char broker_url[23];
    sprintf(broker_url, "mqtt://%.*s", 15, broker_ip);

    // Variables
    esp_mqtt_client_config_t mqtt_cfg =
        {
            .uri = broker_url,
            .event_handle = mqtt_event_handler,
        };

    // Initialize MQTT client
    mqtt_client = esp_mqtt_client_init(&mqtt_cfg);
    esp_mqtt_client_start(mqtt_client);

    // Start mqtt_publish_task
    xTaskCreate(mqtt_publish_task, "mqtt_publish_task", 2048, (void *)input_queue, 10, NULL);
}