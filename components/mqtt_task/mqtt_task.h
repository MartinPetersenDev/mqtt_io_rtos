/**
 * MQTT_IO_RTOS
 * 
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev 
 * Apache License Version 2.0
 * 
 * */

#ifndef MQTT_TASK_H
#define MQTT_TASK_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/gpio.h"
#include "jsmn.h"
#include "../include/common.h"

void get_device_id(char *device_id);
void mqtt_publish_task(void *arg);
esp_err_t mqtt_event_handler(esp_mqtt_event_handle_t event);
void mqtt_app_start(xQueueHandle *json_queue, xQueueHandle *input_queue, char *broker_ip);

#endif /* MQTT_TASK_H */