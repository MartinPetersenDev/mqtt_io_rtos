/**
 * MQTT_IO_RTOS
 * 
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev 
 * Apache License Version 2.0
 * 
 * */

#include "gpio_output_task.h"

/**
 * Helper function for setting output from key-value
 */
bool keyval_to_output(char *msg, int pin)
{
    if (strcmp(msg, ON) == 0)
    {
        gpio_set_level(pin, true);
        return true;
    }
    else if (strcmp(msg, TOGGLE) == 0)
    {
        gpio_set_level(pin, !gpio_get_level(pin));
        return !gpio_get_level(pin);
    }
    else
    {
        gpio_set_level(pin, false);
        return false;
    }
}

/**
 * Helper function to turn json into key/value pair
 */
esp_err_t json_to_keyval(char *json)
{
    // Variables
    jsmn_parser parser;
    jsmntok_t tokens[MAX_TOKENS];

    static struct keyval_t
    {
        char key[MAX_KEY_LENGTH];
        char value[MAX_KEY_LENGTH];
    } keyval[MAX_TOKENS];

    // Parse json
    jsmn_init(&parser);
    int r = jsmn_parse(&parser, json, strlen(json), tokens, MAX_TOKENS);

    // Log received json
    ESP_LOGI(TAG, "[gpio_output_task] Found %i tokens in received json=%s", r, json);

    // proces number of json tokens found
    if (r > 2)
    {
        // extract values from json tokens and insert as key/value pairs in array
        char str[MAX_KEY_LENGTH];

        // clear keyval
        memset(keyval, 0, sizeof(keyval));

        for (int i = 0; i < r; i++)
        {
            if (tokens[i].type == JSMN_STRING)
            {
                // clear string buffer
                memset(str, 0, sizeof(str));

                int start = tokens[i].start;
                int end = tokens[i].end;

                // extract substring from json to str
                for (int j = 0; j < end - start; j++)
                {
                    str[j] = json[start + j];
                }

                // odd-number array member are keys, the rest are values
                if (i % 2 == 1)
                {
                    strcpy(keyval[i].key, str); // store str as key
                }
                else
                {
                    strcpy(keyval[i - 1].value, str); // store str as value
                }
            }
        }

        // process key/value pairs
        for (int i = 0; i < sizeof(keyval) / sizeof(keyval[0]); i++)
        {
            if (strcmp(keyval[i].key, INPUT0) == 0 || strcmp(keyval[i].key, INPUT1) == 0)
            {
                // ignore input messages
            }
            else if (strcmp(keyval[i].key, OUTPUT0) == 0)
            {
                keyval_to_output(keyval[i].value, GPIO_OUTPUT_0);
            }
            else if (strcmp(keyval[i].key, OUTPUT1) == 0)
            {
                keyval_to_output(keyval[i].value, GPIO_OUTPUT_1);
            }
        }
        return ESP_OK;
    }
    else
    {
        // if r <= 0, json parsing failed
        printf("[gpio_output_task] Error in parsed json: ");
        if (r == JSMN_ERROR_INVAL)
        {
            printf("Not enough tokens were provided\n");
        }
        else if (r == JSMN_ERROR_NOMEM)
        {
            printf("Invalid character inside JSON string\n");
        }
        else if (r == JSMN_ERROR_PART)
        {
            printf("The string is not a full JSON packet, more bytes expected\n");
        }
        else
        {
            printf("Invalid json\n");
        }
        return ESP_FAIL;
    }
}

#ifndef TEST
/**
 * Task for setting outputs according to json message
 */
void gpio_output_task(void *arg)
{
    // Setup queue
    xQueueHandle ptr_json_message_queue = (xQueueHandle)arg;

    // Task setup
    const TickType_t xFrequency = 100 / portTICK_RATE_MS;
    TickType_t xLastWakeTime;
    xLastWakeTime = xTaskGetTickCount();

    // Setup json variable
    char json[MAX_JSON_SIZE];

    // Task loop
    while (1)
    {
        if (xQueueReceive(ptr_json_message_queue, &json, 0))
        {
            json_to_keyval(json);
        }

        vTaskDelayUntil(&xLastWakeTime, xFrequency);
    }
}
#endif