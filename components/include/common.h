/**
 * MQTT_IO_RTOS
 * 
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev 
 * Apache License Version 2.0
 * 
 * */

#ifndef COMMON_H
#define COMMON_H

#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include "../../main/app_main.h"

// Struct for gpio input signals
typedef struct input_t
{
    int input_type;     // Input type
    int pin;            // GPIO pin
    bool raw;           // Raw pin value
    bool prev_raw;      // Previous raw pin value, used for deboundinc digital inputs
    bool value;         // Debounced boolean value.
    uint16_t int_value; // Integer value, used for counters and analog
    bool is_changed;    // value has changed (signals debounce is complete)
} input_t;

#define INPUT_IS_GPIO 1
#define INPUT_IS_ADC 2

// define device_config location
#ifdef TEST
#define DEVICE_CONFIG_PATH "./test/spiffs/device_config"
#else
#define DEVICE_CONFIG_PATH "/spiffs/device_config"
#endif

#endif