/**
 * MQTT_IO_RTOS
 * 
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev 
 * Apache License Version 2.0
 * 
 * */

#ifndef GPIO_INPUT_TASK_H
#define GPIO_INPUT_TASK_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/gpio.h"
#include "../include/common.h"

bool debounce_input(input_t *ptr);
void gpio_input_task(void *arg);

// Redefine esp_logging as test stub
#ifdef TEST
#undef ESP_LOGI
#undef ESP_LOGW
#undef ESP_LOGE
#define ESP_LOGI(tag, format, ...) printf("%s\n", format)
#define ESP_LOGW(tag, format, ...) printf("%s\n", format)
#define ESP_LOGE(tag, format, ...) printf("%s\n", format)
#undef portTICK_PERIOD_MS
#define portTICK_PERIOD_MS 1
#endif

#endif /* GPIO_INPUT_TASK_H */