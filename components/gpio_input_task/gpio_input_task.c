/**
 * MQTT_IO_RTOS
 * 
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev 
 * Apache License Version 2.0
 * 
 * */

#include "gpio_input_task.h"

/**
 * Helper function for debouncing gpio input
 */
bool debounce_input(input_t *ptr)
{
    ptr->raw = gpio_get_level(ptr->pin);
    if (ptr->raw == ptr->prev_raw && ptr->value != !ptr->prev_raw)
    {
        ptr->value = !ptr->prev_raw;

        // increment counter
        if (ptr->value)
        {
            // check for integer overflow
            if (ptr->int_value + 1 > UINT16_MAX - 1)
            {
                // wrap around
                ptr->int_value = 1;
            }
            else
            {
                ptr->int_value++;
            }
        }
        ptr->is_changed = true;
    }
    else
    {
        ptr->prev_raw = ptr->raw;
        ptr->is_changed = false;
    }
    return ptr->is_changed;
}

/**
 * Task for handling GPIO inputs
 */
void gpio_input_task(void *arg)
{

    xQueueHandle ptr_gpio_input_queue = (xQueueHandle)arg;

    // Variables
    input_t input_array[SIZE_INPUT_ARRAY];
    input_array[0].pin = GPIO_INPUT_0; // assign input0
    input_array[0].input_type = INPUT_IS_GPIO;
    input_array[1].pin = GPIO_INPUT_1; // assign input1
    input_array[1].input_type = INPUT_IS_GPIO;
    // input_array[2] is adc

    // Task setup
    const TickType_t xFrequency = 25 / portTICK_RATE_MS;
    TickType_t xLastWakeTime;
    xLastWakeTime = xTaskGetTickCount();

    // Task loop
    while (1)
    {
        bool array_updated = false;

        for (int i = 0; i < SIZE_INPUT_ARRAY; i++)
        {
            if (debounce_input(&input_array[i]))
            {
                array_updated = true;
            }
        }

        // Send array to queue if changed
        if (array_updated)
        {
            xQueueSend(ptr_gpio_input_queue, &input_array, 0);
        }

        vTaskDelayUntil(&xLastWakeTime, xFrequency);
    }
}
