/**
 * MQTT_IO_RTOS
 * 
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev 
 * Apache License Version 2.0
 * 
 * */

#include "httpd_task.h"

// Static global variables
static httpd_handle_t server = NULL;
static char config_form[] = "<h3>Configure device</h3><br>"
                            "<div>"
                            "<form action=\"/store_config\" method=\"post\">"
                            "<p>"
                            "<label>Broker IP: </ label>"
                            "<input type=\"text\" id=\"broker\" name=\"broker\" value=\"127.0.0.1\"><br>"
                            "</p>"
                            "<p>"
                            "<input type=\"submit\" value=\"Submit\">"
                            "</p></form>"
                            "</div>";

/* HTTP GET handler */
esp_err_t get_config_form_handler(httpd_req_t *req)
{
    char *buf;
    size_t buf_len;

    /* Read URL query string length and allocate memory for length + 1,
     * extra byte for null termination */
    buf_len = httpd_req_get_url_query_len(req) + 1;
    if (buf_len > 1)
    {
        buf = malloc(buf_len);
        if (httpd_req_get_url_query_str(req, buf, buf_len) == ESP_OK)
        {
            ESP_LOGI(TAG, "Found URL query => %s", buf);
            char param[32];
            /* Get value of expected key from query string */
            if (httpd_query_key_value(buf, "query1", param, sizeof(param)) == ESP_OK)
            {
                ESP_LOGI(TAG, "Found URL query parameter => query1=%s", param);
            }
            if (httpd_query_key_value(buf, "query3", param, sizeof(param)) == ESP_OK)
            {
                ESP_LOGI(TAG, "Found URL query parameter => query3=%s", param);
            }
            if (httpd_query_key_value(buf, "query2", param, sizeof(param)) == ESP_OK)
            {
                ESP_LOGI(TAG, "Found URL query parameter => query2=%s", param);
            }
        }
        free(buf);
    }

    /* Send response with custom headers and body set as the
     * string passed in user context*/
    const char *resp_str = (const char *)req->user_ctx;
    httpd_resp_send(req, resp_str, strlen(resp_str));

    /* After sending the HTTP response the old HTTP request
     * headers are lost. Check if HTTP request headers can be read now. */
    if (httpd_req_get_hdr_value_len(req, "Host") == 0)
    {
        ESP_LOGI(TAG, "Request headers lost");
    }
    return ESP_OK;
}

httpd_uri_t get_config_form = {
    .uri = "/config",
    .method = HTTP_GET,
    .handler = get_config_form_handler,
    .user_ctx = config_form};

/* An HTTP POST handler */
esp_err_t post_config_handler(httpd_req_t *req)
{
    char buf[100];
    int ret, remaining = req->content_len;

    // Clear buffer
    memset(buf, 0, sizeof(buf));

    while (remaining > 0)
    {
        /* Read the data for the request */
        if ((ret = httpd_req_recv(req, buf,
                                  MIN(remaining, sizeof(buf)))) <= 0)
        {
            if (ret == HTTPD_SOCK_ERR_TIMEOUT)
            {
                /* Retry receiving if timeout occurred */
                continue;
            }
            return ESP_FAIL;
        }

        /* Send back the same data */
        remaining -= ret;

        /* Log data received */
        ESP_LOGI(TAG, "New device_config received from httpd: %.*s", ret, buf);

        // Write to file
        ESP_LOGI(TAG, "Writing to device_config\n");
        FILE *f = fopen(DEVICE_CONFIG_PATH, "w");
        if (f == NULL)
        {
            ESP_LOGE(TAG, "Failed to open device_config for writing");
        }
        else
        {
            // Split data at token &
            char *token = strtok(buf, "&");
            while (token != NULL)
            {
                // replace + with space and ASCII < 45 with \0
                for (int i = 0; token[i] != '\0'; i++)
                {
                    if (token[i] == '+')
                    {
                        token[i] = ' ';
                    }
                    else if ((int)token[i] < 45)
                    {
                        token[i] = '\0';
                    }
                }

                fprintf(f, "%s\n", token);
                ESP_LOGI(TAG, "Saved %s to device_config", token);
                token = strtok(NULL, "&");
            }
            fclose(f);
            ESP_LOGI(TAG, "Rebooting...");
            esp_restart();
        }
    }

    // End response
    httpd_resp_send_chunk(req, NULL, 0);
    return ESP_OK;
}

httpd_uri_t post_config = {
    .uri = "/store_config",
    .method = HTTP_POST,
    .handler = post_config_handler,
    .user_ctx = NULL};

void start_webserver(void)
{
    httpd_handle_t server = NULL;
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();

    // Setup event handlers
    ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &connect_handler, &server));
    ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT, WIFI_EVENT_STA_DISCONNECTED, &disconnect_handler, &server));

    // Start the httpd server
    ESP_LOGI(TAG, "Starting server on port: '%d'", config.server_port);
    if (httpd_start(&server, &config) == ESP_OK)
    {
        // Set URI handlers
        ESP_LOGI(TAG, "Registering URI handlers");
        httpd_register_uri_handler(server, &get_config_form);
        httpd_register_uri_handler(server, &post_config);
    }
    else
    {
        ESP_LOGI(TAG, "Error starting server!");
    }
}

//void stop_webserver(httpd_handle_t server)
void stop_webserver(void)
{
    // Stop the httpd server
    httpd_stop(server);
}

void disconnect_handler(void *arg, esp_event_base_t event_base,
                        int32_t event_id, void *event_data)
{
    httpd_handle_t *server = (httpd_handle_t *)arg;
    if (*server)
    {
        ESP_LOGI(TAG, "Stopping webserver");
        stop_webserver();
        *server = NULL;
    }
}

void connect_handler(void *arg, esp_event_base_t event_base,
                     int32_t event_id, void *event_data)
{
    httpd_handle_t *server = (httpd_handle_t *)arg;
    if (*server == NULL)
    {
        ESP_LOGI(TAG, "Starting webserver");
        start_webserver();
    }
}
