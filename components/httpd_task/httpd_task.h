/**
 * MQTT_IO_RTOS
 * 
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev 
 * Apache License Version 2.0
 * 
 * */

#ifndef HTTPD_TASK_H
#define HTTPD_TASK_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/gpio.h"
#include "jsmn.h"
#include "../include/common.h"
#include "esp_spiffs.h"
#include <sys/param.h>
#include <esp_http_server.h>

/**
 * MQTT_IO_RTOS
 * 
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev 
 * Apache License Version 2.0
 * 
 * */

#include "httpd_task.h"

esp_err_t get_config_form_handler(httpd_req_t *req);
esp_err_t post_config_handler(httpd_req_t *req);
void start_webserver(void);
void stop_webserver(void);
void disconnect_handler(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data);
void connect_handler(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data);

#endif /* HTTPD_TASK_H */