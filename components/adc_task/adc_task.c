/**
 * MQTT_IO_RTOS
 * 
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev 
 * Apache License Version 2.0
 * 
 * */

#include "adc_task.h"

void adc_task(void *arg)
{

    xQueueHandle ptr_gpio_input_queue = (xQueueHandle)arg;

    // Variables
    input_t input_array[SIZE_INPUT_ARRAY];
    input_array[2].input_type = INPUT_IS_ADC;
    uint16_t adc_data;
    uint16_t old_adc_data = 0;
    int keepalive = NEXT_KEEPALIVE; // number of cycles before next keepalive

    // Task setup (1 sec)
    const TickType_t xFrequency = 10000 / portTICK_RATE_MS;
    TickType_t xLastWakeTime;
    xLastWakeTime = xTaskGetTickCount();

    // Task loop
    while (1)
    {
        if (ESP_OK == adc_read(&adc_data))
        {

            // If adc_data has changed more than DEAD_ZONE pct or keepalive has expired
            if ((adc_data * 100 > old_adc_data * (100 + DEAD_ZONE)) ||
                (adc_data * 100 > old_adc_data * (100 - DEAD_ZONE)) ||
                keepalive <= 0)
            {
                // update queue with new data
                input_array[2].int_value = adc_data;
                xQueueSend(ptr_gpio_input_queue, &input_array, 0);
                ESP_LOGI(TAG, "adc value %d send to queue\r\n", adc_data);
                old_adc_data = adc_data;
                keepalive = NEXT_KEEPALIVE;
            }
            else
            {
                keepalive--;
            }
        }

        vTaskDelayUntil(&xLastWakeTime, xFrequency);
    }
}
