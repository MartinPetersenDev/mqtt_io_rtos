/**
 * MQTT_IO_RTOS
 * 
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev 
 * Apache License Version 2.0
 * 
 * */

#ifndef GPIO_ADC_TASK_H
#define GPIO_ADC_TASK_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/adc.h"
#include "../include/common.h"

#define NEXT_KEEPALIVE 30 // Send keepalive every no. of task-executions
#define DEAD_ZONE 5       // Deadzone for adc change, in absolute units

void adc_task(void *arg);

#endif /* GPIO_ADC_TASK_H */