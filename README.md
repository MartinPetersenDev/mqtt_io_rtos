[![Coverage Status](https://coveralls.io/repos/gitlab/MartinPetersenDev/mqtt_io_rtos/badge.svg?branch=HEAD)](https://coveralls.io/gitlab/MartinPetersenDev/mqtt_io_rtos?branch=HEAD)

## ABOUT

This is the sourcecode repository for MQTT_IO_RTOS, a FreeRTOS based embedded
system providing remote I/O capabilities via MQTT.

Use cases:
- Used with [Node-RED](https://nodered.org/)
- Datalogging and events with [telegraf](https://www.influxdata.com/time-series-platform/telegraf/)
- [Grafana](https://grafana.com/) dashboard from an [influxDB](https://www.influxdata.com/) instance
- Connected to an industrial PLC through a IEC 61131-1 [MQTT library](https://store.codesys.com/iiot-libraries-sl.html)

The sourcecode in this repository was built using the [ESP8266-RTOS-SDK]((https://docs.espressif.com/projects/esp8266-rtos-sdk/en/latest/get-started/) ).

## BUILD INSTRUCTIONS

### WIFI provisioning
WIFI provisioning is handled by menuconfig, as shown below. 
WIFI provisioning  with the ESP-TOUCH app is planned for a future release.

### Configuration
Setup the ESP8266 RTOS SDK by following these [instructions](https://docs.espressif.com/projects/esp8266-rtos-sdk/en/latest/get-started/) 
and run *menuconfig*
```shell
make menuconfig
```
Set the following parameters:
- Serial flasher config -> Default serial port
- CONFIG_HTTPD_MAX_REQ_HDR_LEN = 2048
- Enable SPIFFS
- CONFIG_SPIFFS_MAX_PARTITIONS = 4
- WIFI settings

### Build and flash firmware
Build and flash the firmware

```shell
make menuconfig
make flash monitor
```

### MQTT provisioning
MQTT broker configuration is entered from an embedded webserver.
When no broker ip is configured the webserver is started at boot.

Broker ip can be configured from the embedded webserver at
http://*device ip*/config

The device will reboot, when the configuration form is submitted

To erase the configuration, issue the following commands
```shell
make erase_flash
make flash monitor
```

MQTT broker security and authentication is planned for a future release.

## BASIC USAGE

Each device sends and receives JSON payload by subscribing to a MQTT topic 
on a MQTT broker.

The default topic is /io followed by the generated device_id and either input
or output. Device_id is generated from a text prefix and the last 3 bytes of
the device MAC adress.

Example topic for device wemos01abcd:

**/io/wemos01abcd/input** *(for inputs published by the device)*

**/io/wemos01abcd/output** *(for outputs subscribed by the device)*

Example payload:

Published by wemos01abcd on topic **/io/wemos01abcd/input**:
```json
{
    "device_id":"wemos01abcd",
    "input0":"on",
    "counter0":123,
    "input1":"off",
    "counter1":0,
    "analog0":1023
}
```

Received by wemos01abcd on topic **/io/wemos01abcd/output** from other_device:
```json
{
    "device_id":"other_device",
    "output0":"on",
    "output1":"off"
}
```

Valid input values:
- **input0** : *"on"* | *"off"*
- **counter0** : *123*
- **analog0** : *2345*

Valid output values:
- **output0** : *"on"* | *"off"* | *"toggle"*

## LICENSE

This software is copyright (c) 2020 Martin Petersen / @MartinPetersenDev 
and released under the Apache License Version 2.0